/*
 * @package   solo
 * @copyright Copyright (c)2014-2020 Nicholas K. Dionysopoulos / Akeeba Ltd
 * @license   GNU General Public License version 3, or later
 */

if (typeof akeeba === 'undefined')
{
	var akeeba = {};
}

akeeba.jQuery = jQuery;