<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

get_header();
$container   = get_theme_mod( 'understrap_container_type' );
?>
<div id="wrapper-content">
	<section class="bg-full bg-full--blog line-banner d-flex align-items-center mb-5" style="background-image: linear-gradient(rgba(255,255,255,.6), rgba(255,255,255,.6)), url('<?php echo get_the_post_thumbnail_url() ; ?>')">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<div class="align-middle d-inline-block text-center w-100 post-title mb-2">
						<h1><?php echo get_the_title(); ?></h1>
					</div>
					<meta itemprop="datePublished" content="<?= get_the_date( 'c' ) ?>">
					<?= iconv("ISO-8859-2","UTF-8",ucfirst(strftime('%d.%m.%Y',strtotime(get_the_date( 'c' )))));?>
				</div>
			</div>
		</div>
	</section>
	<div class="wrapper" id="single-wrapper">

		<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

			<div class="row">
				<div class="col">

					<!-- Do the left sidebar check -->
					<?php //get_template_part( 'global-templates/left-sidebar-check' ); ?>

					<main class="site-main" id="main">

						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'loop-templates/content', 'single' ); ?>

								<?php understrap_post_nav(); ?>

							<?php
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
							?>

						<?php endwhile; // end of the loop. ?>

					</main><!-- #main -->

				</div>
			</div><!-- #primary -->

			<!-- Do the right sidebar check -->
			<?php //get_template_part( 'global-templates/right-sidebar-check' ); ?>

		</div><!-- .row -->

	</div><!-- Container end -->

	</div><!-- Wrapper end -->
</div><!-- Wrapper end -->

<?php get_footer(); ?>
