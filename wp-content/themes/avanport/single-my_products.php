<?php
get_header();
?>

<div class="wrapper" id="wrapper-content">
<?php while (have_posts()) : the_post();
$productLineId = get_field('line');
$productLineId = $productLineId[0];
$linePage = get_page($productLineId);
$lineName = get_field('name', $productLineId);
$variants = Array('a', 'b', 'c');

$modelsList = '';
$modelsQuery = new WP_Query(array(
      'post_type' => 'my_products',
      'order' => 'DESC',
      'meta_query' => array(
          array(
              'key' => 'line',
              'value' => $productLineId,
              'compare' => 'LIKE'
          )
      )
  ));
  if ($modelsQuery->have_posts()){
    global $wp;
    $currentUrl = home_url( $wp->request ).'/';
    while ($modelsQuery->have_posts()){
      $modelsQuery->the_post();
//      echo $currentUrl.'   '.get_permalink().'<br/>';
      $activeClass = $currentUrl == get_permalink() ? 'active' : '';
      $modelsList .= '<a class="select select-model d-inline-block '.$activeClass.'" href="'.get_permalink().'" data-modelid="'.$post->ID.'">'.$post->post_title.'</a>';
    }
}

wp_reset_query();
?>
  <div class="container-fluid">

    <div class="row bg-gray1 py-4">
      <div class="col-12">
        <div class="d-inline-block h-100 align-middle"></div>
          <div class="align-middle d-inline-block w-95">
            <div class="logo-<?php echo $lineName; ?>-bl-m"></div>
          </div>
      </div>
    </div>
    <div class="row bg-gray1 border-2 border-white border-top d-none d-lg-flex">
      <div class="container">
        <div class="row model-sub-menu f-16 f-sc f-sb text-center">
          <div class="col border-white border-2 border-left border-right py-2"><a class="d-block" href="#parameters"><?php _e('[:pl]parametry techniczne[:en]TECHNICAL PARAMETERS[:de]TECHNISCHE DATEN[:]'); ?></a></div>
          <?php if(have_rows('stones', $productLineId)): ?><div class="col border-white border-2 border-right py-2"><a class="d-block" href="#stones"><?php _e('[:pl]wykończenia kamienne[:en][:de][:]'); ?></a></div><?php endif; ?>
          <div class="col border-white border-2 border-right py-2"><a class="d-block" href="#insides"><?php _e('[:pl]wewnętrzna strona drzwi[:en]INTERNAL SIDE OF THE AVANPORT DOOR[:de]INNERE SEITE DER AVANTPORT-TÜR[:]'); ?></a></div>
          <?php if(have_rows('files', $productLineId)): ?><div class="col border-white border-2 border-right py-2"><a class="d-block" href="#download"><?php _e('[:pl]pliki do pobrania[:en]DOWNLOAD THE PDF CATALOGUE[:de]LADEN SIE DEN PDF-KATALOG HERUNTER[:]'); ?></a></div><?php endif; ?>
        </div>
      </div>
    </div>
    <div class="row pb-lg-4">
      <div class="container">
        <div class="row">
          <div class="d-lg-none position-relative model-mobile f-12 f-b">
            <div class="position-absolute text-center">
              <span class="d-inline-block px-3 first"><?php _e('[:pl]wybierz model[:en]model[:de]modell[:]'); ?></span>
              <?php echo $modelsList ?>
            </div>
          </div>
          <div class="col-12 col-lg-10 d-none d-lg-block">
            <div class="f-12 pt-3"><?php _e('[:pl]wybierz model[:en]model[:de]modell[:]'); ?></div>
            <div class="f-16 f-sc f-b pt-2"><?php echo $modelsList ?></div>
          </div>
          <div class="col-12 col-lg-2 pl-0 pl-lg-4" id="variants">
            <div class="f-12 pt-lg-3 px-3 px-lg-0 first"><?php if(have_rows('gallery_b')) {_e('[:pl]wybierz wariant[:en]variation[:de]variant[:]');} ?></div>
            <div class="f-16 f-sc f-b pt-lg-2 mt-lg-1">
            <?php 
            if(have_rows('gallery_b')){echo '<span class="select-variant active px-lg-2" data-variant="a">A</span><span class="select-variant px-lg-2" data-variant="b">B</span>';}
            if(have_rows('gallery_c')){echo '<span class="select-variant px-lg-2" data-variant="c">C</span>';}
            ?>
            </div>  
          </div>
        </div>
      </div>
    </div>
    <div class="row border-gray border-1 border-top border-bottom">
      <div class="container">
        <div class="row" id="gallery">
          <div class="col-12 col-lg-7 border-1 border-gray border-left border-right px-0">
            <?php 
            $fb = false;
            foreach($variants as $v):
            if(have_rows('gallery_'.$v)): 
              $n = 0;
              $haveMoreVariants = have_rows('gallery_b') ? true : false;
              $galleryRows = get_field('gallery_'.$v);
              foreach ($galleryRows as $row) {
                $ids[] = $row['photo'];
              }
              $cache = get_posts(array('post_type' => 'attachment', 'numberposts' => -1, 'post__in' => $ids));
              foreach($galleryRows as $row):    
                $photoId = $row['photo'];
                $photoThumb = wp_get_attachment_image_src($photoId, 'model_thumb')[0];
                $fullPhoto = wp_get_attachment_image_src($photoId, 'page_width')[0];
                $photo = wp_get_attachment_image_src($photoId, 'model_big')[0];
                $pclass = $fb ? 'd-none' : '';
                $dataVariant = $haveMoreVariants ? __('[:pl]wariant [:en]variation [:de]variant [:]').' '.$v : '';
              ?>
            <div id="mb_<?php echo basename($photoThumb, '.jpg'); ?>" class="h-100 text-center big-photo <?php echo $pclass; ?>" >
              <img src="<?php echo $photo; ?>" alt="<?php echo basename($photo, '.jpg'); ?>">
              <a class="plus border-1 border-gray border-top border-left f-22" href="<?php echo $fullPhoto; ?>" data-itemname="<?php echo get_the_title(); ?>" data-variant="<?php echo $dataVariant; ?>">+</a>
            </div>
              <?php $fb = true; $n++; endforeach; endif; endforeach;?>
          </div>
          <div class="col-12 col-lg-5 border-1 border-gray border-right gallery-wrapper">
            <?php
            $fv = false;
            foreach($variants as $v):
              $vclass = $fv ? 'd-none' : '';
              $bclass = $fv ? '' : 'd-lg-block';
            if(have_rows('gallery_'.$v)): 
              $en = 1;
              $allItems = count(get_field('gallery_'.$v));
              ?>
            <div class="gallery row <?php echo $vclass; ?>" id="gallery_<?php echo $v; ?>">
              <div class="gallery-page">
              <?php 
              $galleryRows = get_field('gallery_'.$v);
              foreach($galleryRows as $row): 
                $photoId = $row['photo'];
                $photo = wp_get_attachment_image_src($photoId, 'model_thumb')[0];
                $br = $en % 3 === 0 ? '' : 'border-right';
              ?>
              <div class="gallery-item col-lg-4 px-0 text-center border-1 border-gray border-bottom <?php echo $br; ?>" data-galitem="<?php echo basename($photo, '.jpg'); ?>"><img class="w-100" src="<?php echo $photo; ?>" alt="<?php echo basename($photo, '.jpg'); ?>"></div>
              <?php 
              if(($en % 12 === 0) && $en !== $allItems){echo '</div><div class="gallery-page">';}
              
              $en++; 
              endforeach; 
              $en--;
              if($en % 12 !== 0){
                $ceil = ceil($en / 12);
                $add = ($ceil * 12) - $en;
                
                for($i = 0; $i < $add; $i++){
                  echo '<div class="d-none d-lg-block gallery-item col-4 px-0 invisible"></div>';
                }
              }
              if($en === $allItems){echo '</div>';}
              ?>
            </div>
              <?php if($allItems > 12): ?>
              <span class="d-none <?php echo $bclass; ?> gal-prev border-1 border-gray border-top border-left f-22 switch-<?php echo $v; ?>" id="gal-nav-p-<?php echo $v?>" data-dir="left" data-variant="<?php echo $v; ?>"><</span><span class="d-none <?php echo $bclass; ?> gal-next border-1 border-gray border-top border-left f-22 switch-<?php echo $v; ?> active" id="gal-nav-n-<?php echo $v?>" data-dir="right" data-variant="<?php echo $v; ?>">></span>
              <?php endif; ?>
              
          <?php endif; $fv = true; endforeach;?>
          </div>  
        </div>
      </div>
    </div>
    <div class="row border-1 border-gray border-top border-bottom mt-4">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-6 col-lg-7 f-12 f-sc f-b pt-3 pb-3">
            <?php _e('[:pl]dostępne kolory[:en]colours[:de]farben[:]'); ?>
          </div>
          
            <?php if(($lineName === 'respect') || ($lineName === 'rustical')): ?>
          <div class="col-12 col-md-6 col-lg-5 f-12 f-sc f-b py-3 pb-md-0">
              <?php _e('[:pl]Powyższe modele dostępne we wszystkich kolorach ral[:en]The models above are available in all RAL colours[:de]Die oben genannten Modelle sind in allen RAL-Farben erhältlich.[:]'); ?> 
            <?php else: ?>
          <div class="col-12 col-md-6 col-lg-5 f-12 f-sc f-b pb-4 pb-md-0">
              <div class="row txt-gray h-100">
                <div class="col-3 px-0">
                  <div class="ral ral-9003 h-50"></div>
                  <div class="h-50 text-center pt-1">ral 9003</div>
                </div>
                <div class="col-3 px-0">
                  <div class="ral ral-7004 h-50"></div>
                  <div class="h-50 text-center pt-1">ral 7004</div>
                </div>
                <div class="col-3 px-0">
                  <div class="ral ral-9005 h-50"></div>
                  <div class="h-50 text-center pt-1">ral 9005</div>
                </div>
                <div class="col-3 px-0">
                  <div class="ral ral-7021 h-50"></div>
                  <div class="h-50 text-center pt-1">ral 7021</div>
                </div>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
    <div class="row mb-5" id="parameters">
      <div class="container">
        <div class="row">
          <div class="col-12 my-4">
            <h2 class="text-uppercase f-16 f-lg-22 f-b py-2"><u><?php _e('[:pl]Parametry techniczne[:en]TECHNICAL PARAMETERS[:de]TECHNISCHE DATEN[:]'); ?></u></h2>
          </div>
        </div>
        
        <?php 
        $paramTableId = '';
        $paramQuery = new WP_Query(array('post_type' => 'my_line'));
        if($paramQuery->have_posts()){
          while($paramQuery->have_posts()){
            $paramQuery->the_post();
            if(have_rows('param_table', $post->ID)){
              $paramTableId = $post->ID;
              break;
            }
          }
        }
        wp_reset_query();
        if(have_rows('param_table', $paramTableId)): 
            
            
          while(have_rows('param_table', $paramTableId)): the_row(); ?>
        <div class="row mx-0 bg-gray1 border-2 border-white border-bottom">
          <div class="col-12 col-lg-8 f-16 f-sc f-b py-4 pl-5"><?php echo get_sub_field('title'); ?></div>
          <div class="col-12 col-lg-6 offset-lg-2 f-12 f-sb pb-3"><?php echo get_sub_field('content'); ?></div>
          <div class="col-12 col-lg-4 text-center">
            <?php 
            if(get_sub_field('photo')){
              $photo = get_sub_field('photo');
              $photo = $photo['url'];
              echo '<img class="pb-5" src="'.$photo.'" alt="'.get_sub_field('title').'"/>';
            }
            if(get_sub_field('photo_2')){
              $photo = get_sub_field('photo_2');
              $photo = $photo['url'];
              echo '<img class="pb-5" src="'.$photo.'" alt="'.get_sub_field('title').'-1"/>';
            }
            ?>
          </div>
        </div>
        <?php endwhile; endif; ?>
      </div>
    </div>
    <?php if(have_rows('stones', $productLineId) && !empty(get_field('stones', $productLineId))): 
      ?>
    <div class="row border-1 border-gray border-top" id="stones">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h2 class="text-uppercase f-16 f-lg-22 f-b py-4"><u><?php _e('[:pl]dostępne wykończenia kamienne[:en]available stone finishes[:de]verfügbare Steinausführungen[:]'); ?></u></h2>
          </div>
        </div>
        <div class="row">
          <?php 
          $stonesRows = get_field('stones', $productLineId);
          foreach ($stonesRows as $row) {
                $ids[] = $row['stone_photo'];
              }
              $cache = get_posts(array('post_type' => 'attachment', 'numberposts' => -1, 'post__in' => $ids));
              foreach($stonesRows as $row): ?>
          <div class="col-12 col-md-6">
            <?php 
              $stone_id = $row['stone_photo'];
              $stone = wp_get_attachment_image_src($stone_id, 'stone_thumb')[0];
              echo '<div><img src="'.$stone.'" alt="'.get_sub_field('stone_name').'"/></div><div class="f-16 f-sc f-b pt-2 pb-4">'.get_sub_field('stone_name').'</div>';
            ?>
          </div>
          <?php endforeach ?>
        </div>
      </div>
    </div>
    <?php endif; ?>
    <div class="row border-1 border-gray border-top" id="insides">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h2 class="text-uppercase f-16 f-lg-22 f-b my-5"><u><?php _e('[:pl]Wewnętrzna strona drzwi[:en]INTERNAL SIDE OF THE AVANPORT DOOR[:de]INNERE SEITE DER AVANTPORT-TÜR[:]'); ?></u></h2>
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-lg-8 mx-auto pb-5">
            <?php echo apply_filters('the_content', get_post_field('post_content', INSIDES_ID)); ?>
          </div>
        </div>
        <div class="row">
          <?php 
            if(have_rows('gallery', INSIDES_ID)): 
              $insidesRows = get_field('gallery', INSIDES_ID);
              foreach ($insidesRows as $row) {
                $ids[] = $row['photo'];
              }
              $cache = get_posts(array('post_type' => 'attachment', 'numberposts' => -1, 'post__in' => $ids));
             foreach ($insidesRows as $row):
            $galItemId = $row['photo'];
            $galItemFull = wp_get_attachment_image_src($galItemId, 'in_big')[0];
            $galItem = wp_get_attachment_image_src($galItemId, 'model_thumb')[0];
          ?>          
          <div 
            class="col-4 col-lg-2 border-1 border-gray border-all border py-3 gall-item <?= in_array(get_field('template'), $row['templates']) ? '' : 'd-none'?>"
            data-templates="<?= implode(',', $row['templates']) ?>"
          >
            <a href="<?php echo $galItemFull; ?>" class="gall-item-a" data-itemname="<?php echo $row['model_name']; ?>">
              <div class="f-12 f-sc f-b"><?php echo $row['model_name']; ?></div>
              <div class="text-center">
                <img src="<?php echo $galItem; ?>" alt="<?php echo $row['model_name']; ?>" />
              </div> 
            </a>
          </div>
          <?php endforeach; endif; ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="container">
        <div class="row">
          <div class="col-12 col-lg-8 text-lg-center mx-auto py-5">
            <?php _e('[:pl]Dodatkowo drzwi avanport od wewnętrzenej strony mogą być wyposażone w:[:en]Additionally the internal side of the AVANPORT door may be equipped with:[:de]Zusätzlich kann die AVANPORT-Tür von innen mit den folgenden Funktionalitäten ausgestattet werden:[:]'); ?>
          </div>
        </div>
      </div>
    </div>
    <?php if(have_rows('handrails', INSIDES_ID)): ?>
    <div class="row border-1 border-gray border-top border-bottom">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h2 class="f-16 f-lg-22 f-b py-4"><?php _e('[:pl]Pochwyty[:en]Handle bars[:de]Ziehgriff[:]'); ?></h2>
          </div>
        </div>
      </div>
    </div>
    <div class="row border-1 border-gray border-bottom">
      <div class="container">
        <div class="row">
          <?php 
          $handRows = get_field('handrails', INSIDES_ID);
          foreach ($handRows as $row) {
            $ids[] = $row['photo'];
          }
          $cache = get_posts(array('post_type' => 'attachment', 'numberposts' => -1, 'post__in' => $ids));
          foreach ($handRows as $row): 
            $handPhotoId = $row['photo'];
            $handPhoto = wp_get_attachment_image_src($handPhotoId, 'handrail')[0];
            ?>
          <div class="col-12 col-md-4">
            <div class=" border-1 border-gray border-left border-right">
              <img class="w-100" src="<?php echo $handPhoto; ?>" alt="<?php echo $row['variant']; ?>" />
              <div class="pb-5 d-lg-none">
                <div class="f-16 f-sc f-b pt-2 px-2"><?php echo $row['variant']; ?></div>
                <div class="f-12 f-sb px-2"><?php echo $row['description']; ?></div>
              </div>
            </div>
          </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
    <div class="row border-1 border-gray border-bottom d-none d-lg-flex">
      <div class="container">
        <div class="row">
          <?php while(have_rows('handrails', INSIDES_ID)): the_row(); ?>
          <div class="col-12 col-lg-4 pb-5">
            <div class="f-16 f-sc f-b pt-2"><?php echo get_sub_field('variant'); ?></div>
            <div class="f-12 f-sb"><?php echo get_sub_field('description'); ?></div>
          </div>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
    <?php endif; ?>
    <div class="row border-1 border-gray border-bottom">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h2 class="f-16 f-lg-22 f-b my-4 py-2"><?php _e('[:pl]Klamki[:en]Door handles[:de]Klinken[:]'); ?></h2>
          </div>
        </div>
      </div>
    </div>
    <?php if(have_rows('handles', INSIDES_ID)): 
      $hn = false;
      ?>
    <div class="row border-1 border-gray border-bottom">
      <div class="container">
        <div class="row">
          <?php 
            $handlesRows = get_field('handles', INSIDES_ID);
            foreach ($handlesRows as $row) {
              $ids[] = $row['photo'];
            }
            $cache = get_posts(array('post_type' => 'attachment', 'numberposts' => -1, 'post__in' => $ids));
            foreach ($handlesRows as $row):
            $handleThumbId = $row['photo'];
            $handleThumb = wp_get_attachment_image_src($handleThumbId, 'handle_thumb')[0];
            
          ?>
          <div class="handle col-4 col-lg border-1 border-gray border-left border-right border-bottom border-all pt-5 pb-1" data-handle_name="<?php echo sanitize_title($row['name']); ?>">
            <div><img src="<?php echo $handleThumb; ?>" alt="<?php echo $row['name']; ?>" /></div>
            <div class="f-16 f-sc f-b pt-3"><?php echo $row['name']; ?></div>
          </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
    <div class="row border-1 border-gray border-bottom">
      <div class="container">
        <?php 
        
          foreach ($handlesRows as $row): 
            $handleBigId = $row['photo'];
            $handleBig = wp_get_attachment_image_src($handleBigId, 'andle_big')[0];
            $first = $hn ? 'd-none' : '';
          ?>
        <div class="row handle-big <?php echo $first; ?>" id="handle_<?php echo sanitize_title($row['name']); ?>">
          <div class="col-12 col-lg-7 text-center">
            <div class="h-100 d-inline-block align-middle"></div><div class="d-inline-block align-middle pt-5 pt-lg-0"><img src="<?php echo $handleBig; ?>" alt="<?php echo $row['name']; ?>" /></div>
          </div>
          <div class="col-12 col-lg-5 py-5">
            <div class="row bg-gray1 border-2 border-white border-bottom py-4">
              <div class="col-12 f-16 f-lg-22 f-sc f-b pl-lg-5"><strong><?php echo $row['name']; ?></strong></div>
            </div>
            <div class="row bg-gray1 border-2 border-white border-bottom py-2">
              <div class="col-6 f-16 f-sc f-b pl-lg-5">
                <?php _e('[:pl]rodzaj szyldu[:en]Escutcheon type[:de]Türgriffschild[:]'); ?>
              </div>
              <div class="col-6 f-12 f-sb pl-0 pt-2"><?php echo $row['sign']; ?></div>
            </div>
            <div class="row bg-gray1 border-2 border-white border-bottom py-2">
              <div class="col-6 f-16 f-sc f-b pl-lg-5">
                <?php _e('[:pl]materiał[:en]Material[:de]Material[:]'); ?>
              </div>
              <div class="col-6 f-12 f-sb pl-0 pt-2"><?php echo $row['material']; ?></div>
            </div>
            <div class="row bg-gray1 border-2 border-white border-bottom py-2">
              <div class="col-6 f-16 f-sc f-b pl-lg-5">
                <?php _e('[:pl]kolorystyka[:en]Colours[:de]Farben[:]'); ?>
              </div>
              <div class="col-6 f-12 f-sb pl-0 py-2"><?php echo $row['colors']; ?></div>
            </div>
            <div class="row bg-gray1 border-2 border-white border-bottom py-2">
              <div class="col-6 f-16 f-sc f-b pl-lg-5">
                <?php _e('[:pl]rodzaj rozetki[:en]Type of escutcheon[:de]Rosettenart[:]'); ?>
              </div>
              <div class="col-6 f-12 f-sb pl-0 pt-2"><?php echo $row['rosette']; ?></div>
            </div>
            <div class="row bg-gray1 border-2 border-white border-bottom py-2">
              <div class="col-6 f-16 f-sc f-b pl-lg-5">
                <?php _e('[:pl]materiał[:en]Material[:de]Material[:]'); ?>
              </div>
              <div class="col-6 f-12 f-sb pl-0 pt-2"><?php echo $row['rosette_material']; ?></div>
            </div>
            <div class="row bg-gray1 border-2 border-white border-bottom py-2">
              <div class="col-6 f-16 f-sc f-b pl-lg-5">
                <?php _e('[:pl]kolorystyka[:en]Colours[:de]Farben[:]'); ?>
              </div>
              <div class="col-6 f-12 f-sb pl-0 py-2"><?php echo $row['rosette_colors']; ?></div>
            </div>
          </div>
        </div>
        <?php $hn = true; endforeach; ?>
      </div>
    </div>
    <?php endif; ?>
    <?php if(have_rows('files', $productLineId)): ?>
    <div class="row pb-5" id="download">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h2 class="text-uppercase f-16 f-lg-22 f-b py-4 my-3"><u><?php _e('[:pl]Pliki do pobrania[:en]DOWNLOAD THE PDF CATALOGUE[:de]LADEN SIE DEN PDF-KATALOG HERUNTER[:]'); ?></u></h2>
          </div>
        </div>
        <div class="row">
          <?php  
          $colNr = 0;
            while(have_rows('files', $productLineId)): the_row();
              if(in_array('model', get_sub_field('file_where'))):
                if($colNr % 2 === 0):
            ?>
          <div class="col-12 col-lg-4"></div>
          <?php endif; 
                  $thumb = get_sub_field('catalog_thumb');
                  $thumb = $thumb['sizes']['catalog_thumb'];
            ?>
          <div class="col-12 col-lg-4">
            <a class="row" href="<?php echo get_sub_field('file'); ?>" target="_blank" rel="noopener">
              <div class="catalog-thumb col-12 col-lg-5">
                <img src="<?php echo $thumb; ?>" alt="<?php echo $lineName; ?>" />
              </div>
              <div class="col-12 col-lg-7 align-bottom">
                <div class="h-100 d-inline-block"></div><?php echo get_sub_field('file_name'); ?>
              </div>
            </a>
          </div>
          <?php endif; $colNr++; endwhile;?>
        </div>
      </div>
    </div>
    <?php endif; ?>
    <div class="row bg-gray0 py-md-5">
      <div class="container">
        <div class="row">

            <?php
      if (have_rows('lines', FRONT_PAGE_ID)):
        while (have_rows('lines', FRONT_PAGE_ID)):
          the_row();
          $line = get_sub_field('line');
          $line = $line[0];
          $lineName = get_field('name', $line->ID);
        ?>
        <div class="col-12 col-md-3 line-link">
          <a class="line-link" href="<?php echo get_permalink($line->ID); ?>">
            <div class="row  <?php echo $lineName; ?> mx-lg-0">
              <div class="col-12 col-md-6 h-30 h-md-60 bg-gray1 text-center align-middle px-0">
                <div class="d-inline-block h-100 align-middle"></div>
                <div class="align-middle d-inline-block w-95">
                  <div class="logo-<?php echo $lineName; ?>-bl-s"></div>
                </div>
              </div>
              <div class="col-12 col-md-6 h-45 h-md-60 bg-full" style="background-image:url('<?php echo get_the_post_thumbnail_url($line->ID, 'line_thumb'); ?>'); "></div>
              <div class="col-12 f-lg-16 f-b h-25 h-md-auto pt-2 pt-md-0"><?php echo get_field('type', $line->ID); ?></div>
            </div>
          </a>
        </div>
      <?php  
        endwhile;
      endif;
      ?>
        </div>
      </div>
    </div>
  </div><!-- Container end -->
<?php endwhile; // end of the loop.  ?>
</div><!-- Wrapper end -->

<?php get_footer(); ?>