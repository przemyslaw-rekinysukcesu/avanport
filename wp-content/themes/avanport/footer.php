<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

?>


<div class="wrapper footer bg-black" id="wrapper-footer">

	<div class="container-fluid">

		<div class="row text-white">
      <div class="container">
        <div class="row">
          <div class="col text-right">
            <ul class="social f-10 f-sb text-uppercase pt-lg-4 mt-lg-3 pt-4 pt-lg-0">
              <?php
              if (have_rows('socials', FRONT_PAGE_ID)):
                while (have_rows('socials', FRONT_PAGE_ID)):
                  the_row();
                  $link = get_sub_field('link');
                  $link = strstr($link, 'http') ? $link : 'https://' . $link;
                  echo '<li><a href="' . $link . '" target="_blank" rel="noopener">' . get_sub_field('name') . '</a></li>';
                endwhile;
              endif;
              ?>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-4 col-lg">
            <a class="footer-brand mt-7 mt-md-4 mt-lg-3" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"></a>
          </div>
          <div class="col text-right">
            <?php
            wp_nav_menu(
                    array(
                        'theme_location' => 'footer',
                        'container_class' => '',
                        'container_id' => '',
                        'menu_class' => 'footer-menu f-xl-16 f-sb f-sc mt-6 pt-2 pt-md-5 mt-md-2',
                        'fallback_cb' => '',
                        'menu_id' => 'footer-menu'
//                        'walker' => new understrap_WP_Bootstrap_Navwalker(),
                    )
            );
            $ppolicy = get_field('ppolicy', FRONT_PAGE_ID);
            $ppolicy = $ppolicy[0];
            ?>
            <div class="f-10 f-sb">&copy; 2018 <a class="pl-4" href="<?php echo get_permalink($ppolicy->ID); ?>"><?php echo $ppolicy->post_title; ?></a></div>
          </div>
        </div>
      </div>

		</div><!-- row black end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TZTQSQ7"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- UE LiveChat script -->
<script data-cfasync="false">
    window.civchat = {
      apiKey: "QREI3h",
    };
  </script>
<script data-cfasync="false" src="https://app.userengage.com/widget.js"></script>
<!-- End UE LiveChat script -->         


</body>

</html>

