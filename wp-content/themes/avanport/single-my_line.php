<?php
get_header();
?>

<div class="wrapper" id="wrapper-content">
<?php while (have_posts()) : the_post(); 
$name = get_field('name');
$type = get_field('type');
$lineId = $post->ID;
$bannerImg = get_field('banner');
$bannerImg = $bannerImg['sizes']['page_width']

?>
  <div class="container-fluid">

    <div class="row">

      <div class="col-12 bg-full line-banner" style="background-image: url('<?php echo $bannerImg; ?>')">
        <div class="d-inline-block h-100 align-middle"></div>
          <div class="align-middle d-inline-block w-95">
            <div class="logo-<?php echo $name; ?>-bl-l invert"></div>
          </div>
      </div>

    </div>
    <div class='row py-4 py-lg-5 <?php echo $name; ?>'>
      
    </div>
    
    <div class='row'>
      <div class='container'>
        <div class='row'>
          <div class='col-12 col-lg-8 mx-lg-auto mt-5 mt-lg-6'>
            <h2 class="f-lg-22 f-b mt-lg-6 pt-2 pb-3"><?php echo $type; ?></h2>
            <?php the_content(); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-lg-8 mx-lg-auto mt-5 pt-4">
            <?php 
            if (have_rows("files")): while (have_rows("files")): the_row();
//            var_dump(get_sub_field('file'));
              if(get_sub_field('file') && in_array('line', get_sub_field('file_where'))): 
                $thumb = get_sub_field('catalog_thumb');
                $thumb = $thumb['sizes']['catalog_thumb'];
                ?>
            <div class="row">
              <div class="col-12 col-lg-5">
                <a class="row" href="<?php echo get_sub_field('file'); ?>" target="_blank" rel="noopener">
                  <div class="catalog-thumb col-12 col-lg-6">
                    <img src="<?php echo $thumb; ?>" alt="<?php echo $name; ?>">
                  </div>
                  <div class="col-12 col-lg-6 align-bottom f-sb f-sc">
                    <div class="h-100 d-inline-block"></div><?php echo get_sub_field('file_name'); ?></div>
                </a>
              </div>
            </div>
                <?php
                endif;
              endwhile;
            endif;
            ?>                  
          </div>
          
        </div>
        <div class="row models">
          <div class="container">
            <div class="row">
              <div class="col-12">
                <h2 class="f-lg-22 f-b my-4 py-5"><?php _e('[:pl]Modele[:en]Models[:de]Modelle[:]')?></h2>
              </div>
            </div>
            <div class="row">
              <?php 
              $modelsQuery = new WP_Query(array(
                  'post_type'=>'my_products',
//                  'orderby'=> 'title',
                  'order'=> 'DESC',
                  'meta_query'=>array(
                      array(
                          'key'=>'line',
                          'value'=> $lineId,
                          'compare'=> 'LIKE'
                      )
                  )
              ));
              
              if($modelsQuery->have_posts()): while ($modelsQuery->have_posts()): $modelsQuery->the_post(); ?>
              <div class="col-6 col-md-4 col-lg-3 model_thumb_big bg-full my-md-4 my-lg-0 py-2" style="background-image: url('<?php echo get_the_post_thumbnail_url($post, 'model_thumb_big') ?>');">
                <a href="<?php echo get_permalink(); ?>">
                  <div class="octagon f-12 f-b f-sc"><?php echo $post->post_title; ?></div>
                  <!--<img src="" alt="<?php echo $name.' - '.$post->title; ?>" />-->
                </a>
              </div>
              <?php
              endwhile;
            endif;
              ?>
            </div>
            <?php if($name === 'respect'): ?>
            <div class="row mt-6 pt-4 mb-5 pb-3">
              <div class="table-row bg-gray1">
                <div class="col-4 col-lg-2 f-16 f-b f-sc pt-lg-4 pl-lg-5 pb-lg-4"><?php _e('[:pl]Model[:en]Model[:de]Modell[:]')?></div><div class="col-8 col-lg-5 f-16 f-b f-sc pt-lg-4 border-2 border-white border-left pl-lg-4"><?php _e('[:pl]wariant a[:en]variety a[:de]variante a[:]')?></div><div class="d-none d-md-block col-8 col-lg-5 f-16 f-b f-sc pt-lg-4 border-2 border-white border-left pl-lg-4"><?php _e('[:pl]wariant b[:en]variety b[:de]variante b[:]')?></div>
              </div>
              <?php if($modelsQuery->have_posts()): while ($modelsQuery->have_posts()): $modelsQuery->the_post(); 
              ?>
              <div class="table-row bg-gray1 py-1">
                <a href="<?php echo get_permalink(); ?>">
                  <div class="col-4 col-lg-2 f-14 f-b f-sc pl-lg-5"><?php echo $post->post_title; ?></div><div class="col-8 col-lg-5 f-12 pl-lg-4 pt-2"><?php the_field('characteristic_a') ?></div><div class="d-none d-md-block col-8 col-lg-5 f-12 pl-lg-4 pt-2"><?php the_field('characteristic_b') ?></div>
                </a>
              </div>
              <?php
              endwhile;
            endif; ?>
              <div class="d-md-none table-row bg-gray1">
                <div class="col-4 col-lg-2 f-16 f-b f-sc pt-lg-4 pl-lg-5 pb-lg-4"><?php _e('[:pl]Model[:en]Model[:de]Modell[:]')?></div><div class="col-8 col-lg-5 f-16 f-b f-sc pt-lg-4 border-2 border-white border-left pl-lg-4"><?php _e('[:pl]wariant b[:en]variety b[:de]variante b[:]')?></div>
              </div>
            <?php if($modelsQuery->have_posts()): while ($modelsQuery->have_posts()): $modelsQuery->the_post(); 
              ?>
              <div class="d-md-none table-row bg-gray1 py-1">
                <a href="<?php echo get_permalink(); ?>">
                  <div class="col-4 col-lg-2 f-14 f-b f-sc pl-lg-5"><?php echo $post->post_title; ?></div><div class="col-8 col-lg-5 f-12 pl-lg-4 pt-2"><?php the_field('characteristic_b') ?></div>
                </a>
              </div>
              <?php
              endwhile;
            endif; ?>
            <?php wp_reset_query();
              ?>
            </div>
            <?php endif; ?>
            <?php if($name === 'balance' && have_rows('model', $lineId)): ?>
            <div class="row mt-6 pt-4 mb-5 pb-3">
              <div class="table-row bg-gray1">
                <div class="col-12 f-12 f-md-16 f-sc pt-md-4 pl-lg-5 pb-lg-4">
                  <?php echo get_field('balance_text', $lineId) ?>
                </div>
              </div>

              <div class="table-row bg-gray1 f-b">
                <div class="col-4 f-12 f-md-16 f-sc pb-5 pt-4 py-lg-4 pl-lg-5 px-2">
                  <?php _e('[:pl]Model[:en]Model[:de]Modell[:]')?>
                </div>
                <div class="col-2 px-0 f-12 f-md-16 f-sc border-2 border-white border-left">
                  <div class="ral-9003 h-50 border-2 border-white border-bottom"></div>
                  <div class="h-50 pt-2 px-lg-3 text-center">RAL 9003</div>
                </div>
                <div class="col-2 px-0 f-12 f-md-16 f-sc border-2 border-white border-left">
                  <div class="ral-7004 h-50 border-2 border-white border-bottom"></div>
                  <div class="h-50 pt-2 px-lg-3 text-center">RAL 7004</div>
                </div>
                <div class="col-2 px-0 f-12 f-md-16 f-sc border-2 border-white border-left">
                  <div class="ral-9005 h-50 border-2 border-white border-bottom"></div>
                  <div class="h-50 pt-2 px-lg-3 text-center">RAL 9005</div>
                </div>
                <div class="col-2 px-0 f-12 f-md-16 f-sc border-2 border-white border-left">
                  <div class="ral-7021 h-50 border-2 border-white border-bottom"></div>
                  <div class="h-50 pt-2 px-lg-3 text-center">RAL 7021</div>
                </div>
              </div>

              <?php while(have_rows('model', $lineId)): the_row(); ?>

              <div class="table-row bg-gray1 f-b">
                <div class="col-4 f-12 f-md-16 f-sc pt-2 pl-lg-5 pb-2 px-2">
                  <?php 
                    $model = get_sub_field('model_name');
                    $model = $model[0];
                    echo get_the_title($model);
                  ?>
                </div>
                <div class="col-2 f-22 px-0 pt-2">
                  <?php if(in_array('ral-9003', get_sub_field('ral'))){echo '<ul><li></li></ul>';} ?>
                </div>
                <div class="col-2 f-22 px-0 pt-2">
                  <?php if(in_array('ral-7004', get_sub_field('ral'))){echo '<ul><li></li></ul>';} ?>
                </div>
                <div class="col-2 f-22 px-0 pt-2">
                  <?php if(in_array('ral-9005', get_sub_field('ral'))){echo '<ul><li></li></ul>';} ?>
                </div>
                <div class="col-2 f-22 px-0 pt-2">
                  <?php if(in_array('ral-7021', get_sub_field('ral'))){echo '<ul><li></li></ul>';} ?>
                </div>
              </div>

              <?php endwhile; ?>
            </div>
            <?php endif; ?>
            
            <?php if($name === 'eternal' && have_rows('model', $lineId)): ?>
            <div class="row mt-6 pt-4 mb-5 pb-3">
              <div class="table-row bg-gray1 f-b border-bottom-0">
                <div class="col-4 f-12 f-md-16 f-sc pb-5 pt-4 py-lg-4 pl-lg-5 px-2">
                  <?php _e('[:pl]Model[:en]Model[:de]Modell[:]')?>
                </div>
                <div class="col-2 px-0 f-12 f-md-16 f-sc border-2 border-white border-left">
                  <div class="ral-9003 h-50 border-2 border-white border-bottom"></div>
                  <div class="h-50 pt-2 px-lg-3 text-center">RAL 9003</div>
                </div>
                <div class="col-2 px-0 f-12 f-md-16 f-sc border-2 border-white border-left">
                  <div class="ral-7004 h-50 border-2 border-white border-bottom"></div>
                  <div class="h-50 pt-2 px-lg-3 text-center">RAL 7004</div>
                </div>
                <div class="col-2 px-0 f-12 f-md-16 f-sc border-2 border-white border-left">
                  <div class="ral-9005 h-50 border-2 border-white border-bottom"></div>
                  <div class="h-50 pt-2 px-lg-3 text-center">RAL 9005</div>
                </div>
                <div class="col-2 px-0 f-12 f-md-16 f-sc border-2 border-white border-left">
                  <div class="ral-7021 h-50 border-2 border-white border-bottom"></div>
                  <div class="h-50 pt-2 px-lg-3 text-center">RAL 7021</div>
                </div>
              </div>
              <?php 
              $stonesArray = Array(
                  'black_line' => 'black line',
                  'steel_grey' => 'steel grey',
                  'multi_colour' => 'multi colour',
                  'indian_autumn' => 'indian autumn',
                  'autumn_rustic' => 'autumn rustic',
                  'south_grey' => 'south grey'
              )
              ?>
              <?php while(have_rows('model', $lineId)): the_row(); ?>
              <div class="table-row bg-gray1 f-b border-2 border-white border-top">
                <div class="col-4 f-12 f-md-16 f-sc py-2 py-lg-4 pl-lg-5 px-2">
                  <?php 
                    $model = get_sub_field('model_name');
                    $model = $model[0];
                    echo get_the_title($model);
                    $n = 0;
                    foreach(get_sub_field('variant') as $variant){
                      if($variant !== '0'){
                        if($n > 0) {echo ' -';}
                        echo ' '.$variant;
                        $n++;
                      }
                    }
                  ?>
                </div>
              </div>
              <?php foreach($stonesArray as $k => $v): 
                if(!empty(get_sub_field($k.'_ral'))):
                ?>
              <div class="table-row border-0 bg-gray1 f-b">
                <div class="col-4 f-12 f-md-16 f-sc pt-2 pl-lg-5 pb-2 px-2">
                  <?php echo $v; ?>
                </div>
                <div class="col-2 f-22 px-0 pt-2">
                  <?php if(in_array('ral-9003', get_sub_field($k.'_ral'))){echo '<ul><li></li></ul>';} ?>
                </div>
                <div class="col-2 f-22 px-0 pt-2">
                  <?php if(in_array('ral-7004', get_sub_field($k.'_ral'))){echo '<ul><li></li></ul>';} ?>
                </div>
                <div class="col-2 f-22 px-0 pt-2">
                  <?php if(in_array('ral-9005', get_sub_field($k.'_ral'))){echo '<ul><li></li></ul>';} ?>
                </div>
                <div class="col-2 f-22 px-0 pt-2">
                  <?php if(in_array('ral-7021', get_sub_field($k.'_ral'))){echo '<ul><li></li></ul>';} ?>
                </div>
              </div>
              <?php endif; endforeach; ?>

              <?php endwhile; ?>
            </div>
            <?php endif; ?>
            
            
          </div>
        </div>
      </div>
    </div>

  </div><!-- Container end -->
<?php endwhile; // end of the loop.  ?>
</div><!-- Wrapper end -->

<?php get_footer(); ?>