<?php
/**
 * Understrap functions and definitions
 *
 * @package understrap
 */


//@ini_set('upload_max_size', '300M');
//@ini_set('post_max_size', '300M');
//@ini_set('max_execution_time', '300');

/**
 * Initialize theme default settings
 */
require get_template_directory() . '/inc/theme-settings.php';

/**
 * Theme setup and custom theme supports.
 */
require get_template_directory() . '/inc/setup.php';

/**
 * Register widget area.
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Enqueue scripts and styles.
 */
require get_template_directory() . '/inc/enqueue.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/pagination.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Custom Comments file.
 */
require get_template_directory() . '/inc/custom-comments.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load custom WordPress nav walker.
 */
require get_template_directory() . '/inc/bootstrap-wp-navwalker.php';

/**
 * Load Editor functions.
 */
require get_template_directory() . '/inc/editor.php';

//ACF REPEATER
if (!class_exists('acf_repeater_plugin')) {
  include_once( get_stylesheet_directory() . '/inc/acf-repeater/acf-repeater.php');
}

/**
 * sierotki na wlasnych polach
 */
function orphans($content) {
  if (!class_exists('iWorks_Orphan')) {
    return $content;
  }
  $orphan = new iWorks_Orphan();
  return $orphan->replace($content);
}

add_filter('the_content', 'my_content', 20);

function my_content($content) {
  return orphans($content);
}

/**
 * custom image sizes
 */
require get_template_directory() . '/inc/image-sizes.php';

/**
 * custom post types
 */
require get_template_directory() . '/inc/customptype.php';

$lines = new CustomPostType('Linie', 'line', 'line', 5);
$products = new CustomPostType('Modele', 'products', 'products', 5);


/**
 * ajax calls
 */
require get_template_directory() . '/inc/ajax.php';


//
//google api
//

function my_acf_google_map_api($api) {

  $api['key'] = 'AIzaSyClDKPwCkcsDt6pR33ocMedGoUWMvolH1U';

  return $api;
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

//
//add langs to main menu
//

add_filter('wp_nav_menu_main_menu_items', 'add_lang_to_nav', 10, 2);

function add_lang_to_nav($items, $args) {
  ob_start();
  echo qtranxf_generateLanguageSelectCode('short');
  $langSwitch = ob_get_contents();
  ob_end_clean();
  $items .= '<li class="mobile-lang-menu d-lg-none">'.$langSwitch.'</li>';
  return $items;
}

require get_template_directory() . '/inc/theme-updater.php';

/**
 * Custom Constants.
 */
require get_template_directory() . '/inc/constants.php';


function wpb_adding_scripts() {
	wp_register_script('ownjs', get_template_directory_uri(). '/js/own.js', _FILE_);
	wp_enqueue_script('ownjs');
}
add_action ('wp_enqueue_scripts', 'wpb_adding_scripts');
