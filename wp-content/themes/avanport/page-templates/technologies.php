<?php
/**
 * Template Name: Technologie
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>
<div class="wrapper" id="wrapper-content">
  <div class="container-fluid">
      <?php
      $obj = get_field_object('rows');
      $horizontalCount = substr_count(serialize($obj), '"row_type";s:16:"photo_horizontal"');
      $horizontalNr = 0;
      if (have_rows('rows')):
        while (have_rows('rows')):
          the_row();
          $rowType = get_sub_field('row_type');
          $rowBg = '';
          $txtClass = 'col-lg-8';
          $bgfixed = '';
          if($rowType === 'photo'){
            $photo = get_sub_field('photo');
            $rowBg = $photo['sizes']['page_width'];
            $txtClass = 'col-lg-12 text-lowercase txt-white text-center f-22 f-lg-80';
            $bgfixed = 'bg-fixed';
          }
          
          if($rowType === 'photo_horizontal'): 
            $smallPhoto = get_sub_field('photo');
            $smallPhoto = $smallPhoto['sizes']['line_photo'];
            $rowClass = $horizontalNr == $horizontalCount -1 ? 'mb-6' : '';
          ?>

    <div class="row <?php echo $rowClass;?>">
      <div class="container bg-gray1">
        <div class="row">
          <?php if($horizontalNr % 2 === 0): ?>
          <div class="col-12 col-lg-6 f-12 px-5 py-5">
            <?php echo orphans(get_sub_field('text')); ?>
          </div>
          <div class="col-12 col-lg-6 bg-full py-6 py-md-7 py-lg-0 bg-parallax" data-speed=".3" data-start=".7" style="background-image: url('<?php echo $smallPhoto;?>')"></div>
          <?php else: ?>
          <div class="col-12 col-lg-6 bg-full py-6 py-md-7 py-lg-0 d-none d-lg-block bg-parallax" data-speed=".3" data-start=".7" style="background-image: url('<?php echo $smallPhoto; ?>')"></div>
          <div class="col-12 col-lg-6 f-12 px-5 py-5">
            <?php echo orphans(get_sub_field('text')); ?>
          </div>
          <div class="col-12 col-lg-6 bg-full py-6 py-lg-0 d-lg-none bg-parallax" data-speed=".3" data-start=".7" style="background-image: url('<?php echo $smallPhoto; ?>')"></div>
          <?php endif; ?>
        </div>
      </div>
    </div>
          <?php
          $horizontalNr++;
          else: 
        ?>
        
    <div class="row h-100v bg-white bg-full <?php echo $bgfixed; ?>" style="background-image: url('<?php echo $rowBg; ?>')">
      <div class="col-12 text-center align-middle">
        <div class="d-inline-block h-100 align-middle"></div>
        <div class="container align-middle d-inline-block">
          <div class="row">
            <div class="col-12 mx-auto text-left py-5 py-lg-0 <?php echo $txtClass; ?>"><?php echo orphans(get_sub_field('text')); ?></div>
          </div>
        </div>
      </div>
    </div>
    <?php    
          endif;
        ?>
      <?php  
        endwhile;
      endif;
      ?>

  </div><!-- Container end -->

</div><!-- Wrapper end -->
<?php endwhile; // end of the loop.  ?>
<?php get_footer(); ?>