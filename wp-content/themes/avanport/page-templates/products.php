<?php
/**
 * Template Name: Produkty
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */
get_header();
$query = new WP_Query(array(
    'post_type' => 'my_line'
));
?>

  <div class="wrapper" id="wrapper-content">
<?php while ($query->have_posts()) : $query->the_post(); 
$lineName = get_field('name', $post->ID);
?>
    <div class="container-fluid" id="content">
      <div class="row h-75v <?php echo $lineName; ?>">
        <a class="col-12 col-xl-6 h-50 h-lg-100 bg-gray1 text-center align-middle line-link" href="<?php echo get_permalink($post->ID); ?>">
          <div class="d-inline-block h-100 align-middle"></div>
          <div class="align-middle d-inline-block w-95">
            <div class="logo-<?php echo $lineName; ?>-l"></div>
            <div class="f-lg-18 f-b pt-4"><?php echo get_field('type', $post->ID); ?></div>
          </div>
        </a>
        <div class="col-12 col-xl-6 h-50 h-lg-100 bg-full" style="background-image:url('<?php echo get_the_post_thumbnail_url($post->ID, 'line_photo'); ?>'); "></div>
      </div>
      <div class="row bg-white">
        <div class="container">
            <div class="row">
              <div class="col-12 bg-white py-5 py-lg-7">
                <?php the_content(); ?>
              </div>
            </div>
          </div>
      </div>
      <div class="row py-5 py-lg-6 <?php echo $lineName; ?>"></div>
    </div><!-- Container end -->
    
<?php 
      endwhile; // end of the loop.
      wp_reset_query();
      ?>
  </div><!-- Wrapper end -->

<?php get_footer(); ?>