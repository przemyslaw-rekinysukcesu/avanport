<?php
/**
 * Template Name: Contact
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */
get_header();



$shops = [
  'wro' => [
    'name' => 'Wrocław',
    'shops' => []
  ],
  'krk' => [
    'name' => 'Kraków',
    'shops' => []
  ],
  'kat' => [
    'name' => 'Katowice',
    'shops' => []
  ],
  'szc' => [
    'name' => 'Szczecin',
    'shops' => []
  ],
];

foreach(get_field('markers') as $shopData) {
  $shops[$shopData['city']]['shops'][] = $shopData;
}
?>


<?php while (have_posts()) : the_post(); ?>
<div id="wrapper-content">
  <div class="container-fluid">
    <div class="row border-1 border-gray border-top border-bottom">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h2 class="f-22 my-4 py-2 f-b"><?php the_title(); ?></h2>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-lg-6" id="gmap"></div>
      <div class="col-12 col-lg-6 pl-lg-6 py-6">
        <?php the_content(); ?>
      </div>
    </div>
    <div class="row bg-gray1">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h2 class="f-22 my-4 py-2 f-b"><?php _e('[:pl]Napisz[:en]Write[:de]Schreiben Sie uns eine E-Mail.[:]'); ?></h2>
            <?php _e('[:pl]Masz pytania?[:en]Do you have any questions?[:de]Haben Sie Fragen?[:]'); ?><br>
            <?php _e('[:pl]Chcesz bliżej poznać drzwi <strong>avanport</strong>?[:en]Do you want to learn more about <strong>AVANPORT</strong> doors?[:de]Wollen Sie mehr über <strong>AVANPORT</strong>-Türen erfahren?[:]'); ?><br>
            <?php _e('[:pl]Skontaktuj się z nami[:en]Contact us![:de]Kontaktieren Sie uns![:]'); ?><br>
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-lg-8 mx-lg-auto pt-5 pb-6">
            <?php echo do_shortcode(get_field('cf7_shortcode')); ?>
          </div>
        </div>
      </div>
    </div>
    <?php if (have_rows('partners')): ?>
    <div class="row border-1 border-gray border-bottom">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h2 class="f-22 my-4 py-2 f-b"><?php _e('[:pl]Punkty partnerskie[:en]Our partners[:de]Partner-punkte[:]'); ?></h2>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="container">
        <div class="row">
          <div class="col-12 col-lg-8 mx-lg-auto py-6">
            <div class="row">
          <?php while (have_rows('partners')): the_row(); ?>
              <div class="col-12 col-lg-6 pb-5">
                <?php echo get_sub_field('partner'); ?>
              </div>
            <?php endwhile; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php endif; ?>
    <?php if(have_rows('faq')): ?>
    <div class="row border-1 border-gray border-top border-bottom">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h2 class="f-22 my-4 py-2 f-b">FAQ</h2>
          </div>
        </div>
      </div>
    </div>
    <?php while(have_rows('faq')): the_row(); ?>
    <div class="row border-1 border-gray border-bottom">
      <div class="container">
        <div class="row">
          <div class="col-12 col-lg-10 offset-lg-2">
            <div class="row">
              <div class="col-11 col-lg-10">
                <div class="question f-b pt-4 pb-2">
                  <?php echo get_sub_field('question'); ?>
                </div>
                <div class="answer pb-4">
                  <?php echo get_sub_field('answer'); ?>
                </div>
              </div>
              <div class="col-1 col-lg-2 text-right border-1 border-gray border-right">^</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php endwhile; ?>
    <div class="row py-4"></div>
    <?php  endif; ?>
  </div><!-- Container end -->
 
<script type="text/javascript">
    <?php 
      $markers = json_encode(get_field('markers'));
    ?>
      function initMap() {
          var markers = JSON.parse('<?= $markers ?>');
          var bounds = new google.maps.LatLngBounds()
          var mapOptions = {
              scrollwheel: false,
              zoom: 14,
              center: new google.maps.LatLng(52.16, 16.52),
              rotateControl: false,
              streetViewControl: false,
              styles: <?php echo get_field('gmap_style'); ?>
          }
          var map = new google.maps.Map(document.getElementById('gmap'), mapOptions);

          var infoWindows = [];
          markers
            .map(function (m) { 
                return {
                  lat: parseFloat(m.map.lat),
                  lng: parseFloat(m.map.lng),
                  name : m.name,
                  address : m.address
                }
            })
            .forEach(function (m) {
                var point = new google.maps.LatLng(m.lat, m.lng);

                var infowindow = new google.maps.InfoWindow({
                  content: '<strong>' + m.name + '</strong></br>' + m.address
                });

                infoWindows.push(infowindow);

                var marker = new google.maps.Marker({
                    position: point,
                    map: map,
                    title: 'Avanport',
                    icon: '<?php echo get_template_directory_uri(); ?>/img/gmap-ico.png'
                })

                marker.addListener('click', function() {
                  infoWindows.forEach(function (iw) {
                    iw.close();
                  })
                  infowindow.open(map, marker);
                })

                bounds.extend(point);
            })

          map.fitBounds(bounds);
      }

  //        google.maps.event.addDomListener(window, 'load', initialize);

  </script>
 <script async defer
          src="https://maps.googleapis.com/maps/api/js?key=<?php echo get_field('gmap_apikey'); ?>&callback=initMap">
  </script>
<?php endwhile; // end of the loop.   ?>
</div>
<?php get_footer(); ?>
