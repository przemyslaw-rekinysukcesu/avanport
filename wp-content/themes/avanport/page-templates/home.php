<?php
/**
 * Template Name: Home
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */
get_header();
?>
<?php while (have_posts()) : the_post(); 
$bannerImg = get_field('banner_photo');
$bannerImg = $bannerImg['sizes']['page_width'];

?>
<div class="wrapper" id="wrapper-content">

  <div class="container-fluid" id="content">

    <div class="row border-1 border-white border-bottom">
      <div class="col-12 video h-100v bg-full position-relative" style="background-image: url('<?php echo $bannerImg; ?>')">
        <!-- <video autoplay loop class="row w-100 h-100 position-absolute d-none d-lg-flex">
          <source src="<?php the_field('banner'); ?>" type="video/mp4">
          Your browser does not support the video tag.
        </video> -->
	<!-- new video -->
	<div style="background-color: #000; width: 100%; height: 100%; left: 0;" class="d-none d-lg-flex position-absolute ">
		<iframe class="position-absolute" src="https://www.youtube.com/embed/tM6zXrYo-o4?rel=0&amp;controls=0&amp;showinfo=0&amp;autoplay=1&amp;mute=1&amp;loop=1&amp;playlist=tM6zXrYo-o4" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" width="" style="left: 0;top: 50%;height: 56vw;width: 100vw;transform: translate(0%, -50%);/* background-color: #000; */"></iframe>
	</div>
        <!-- end new video -->
	<div class="banner-text w-95 h-100 position-absolute align-middle">
          <div class="d-inline-block h-100 align-middle"></div><h1 class="d-inline-block f-40 f-xl-100 text-center text-white align-middle w-100"><?php the_field('banner_text'); ?></h1>
        </div>
        <div class="container h-100">
          <div class="row h-100">
            <div class="col-12 position-relative">
              <ul class="social home-social f-10 f-sb text-uppercase">
                  <?php
                  if (have_rows('socials')):
                    while (have_rows('socials')):
                      the_row();
                      $link = get_sub_field('link');
                      $link = strstr($link, 'http') ? $link : 'https://' . $link;
                      echo '<li><a href="' . $link . '" target="_blank" rel="noopener">' . get_sub_field('name') . '</a></li>';
                    endwhile;
                  endif;
                  ?>
                </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
      <?php
      if (have_rows('lines')):
        while (have_rows('lines')):
          the_row();
          $line = get_sub_field('line');
          $line = $line[0];
          $lineName = get_field('name', $line->ID);
        ?>
    <div class="row h-100v <?php echo $lineName; ?>">
      <a class="col-12 col-xl-6 h-50 h-lg-85 bg-gray1 text-center align-middle line-link" href="<?php echo get_permalink($line->ID); ?>">
        <div class="d-inline-block h-100 align-middle"></div>
        <div class="align-middle d-inline-block w-95">
          <div class="logo-<?php echo $lineName; ?>-l"></div>
          <div class="f-lg-18 f-b pt-4"><?php echo get_field('type', $line->ID); ?></div>
        </div>
      </a>
      <div class="col-12 col-xl-6 h-50 h-lg-85 bg-full bg-parallax" data-speed=".2" data-start=".5" style="background-image:url('<?php echo get_the_post_thumbnail_url($line->ID, 'line_photo'); ?>'); "></div>
    </div>
      <?php  
        endwhile;
      endif;
      ?>


  </div><!-- Container end -->

</div><!-- Wrapper end -->
<?php endwhile; // end of the loop.  ?>
<?php get_footer(); ?>