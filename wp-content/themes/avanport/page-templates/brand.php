<?php
/**
 * Template Name: Marka
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>
<div class="wrapper" id="wrapper-content">
  <div class="container-fluid">
      <?php
      if (have_rows('row')):
        while (have_rows('row')):
          the_row();
          $rowType = get_sub_field('row_type');
          $rowBg = '';
          $txtClass = '';
          $bgfixed = '';
          if($rowType === 'photo'){
            $photo = get_sub_field('photo');
            $rowBg = $photo['sizes']['page_width'];
            $txtClass = 'txt-white f-22 f-lg-36';
            $bgfixed = 'bg-fixed';
          }
          
        ?>
    <div class="row h-100v bg-white bg-full <?php echo $bgfixed; ?>" style="background-image: url('<?php echo $rowBg; ?>')">
      <div class="col-12 text-center align-middle">
        <div class="d-inline-block h-100 align-middle"></div>
        <div class="container align-middle d-inline-block">
          <div class="row">
            <div class="col-12 col-lg-8 mx-auto text-left py-5 py-lg-0 <?php echo $txtClass; ?>"><?php echo orphans(get_sub_field('text')); ?></div>
          </div>
        </div>
      </div>
    </div>
      <?php  
        endwhile;
      endif;
      ?>

  </div><!-- Container end -->

</div><!-- Wrapper end -->
<?php endwhile; // end of the loop.  ?>
<?php get_footer(); ?>