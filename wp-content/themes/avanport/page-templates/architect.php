<?php
/**
 * Template Name: Dla Architekta
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */
get_header();
$linesQuery = new WP_Query(array(
    'post_type' => 'my_line'
));
$ppolicy = get_field('ppolicy', FRONT_PAGE_ID);
$ppolicy = $ppolicy[0];

global $wp;
$currentUrl = home_url($wp->request) . '/';
?>
<div id="wrapper-content">
  <div class="container-fluid">

    <div class="row bg-gray1">
      <div class="container">
        <div class="row">
          <div class="col-12 col-lg-3 offset-lg-2 f-16 f-b f-sc my-4 pt-3 pb-lg-3 f-lg-20">
            <?php _e('[:pl]Zapisz się do newslettera dla architektów[:en]Subscribe to the architect newsletter[:de]Abonnieren Sie den Newsletter für Architekten.[:]'); ?>
          </div>
          <div class="col-12 col-lg-5 px-lg-0">
            <div class="mt-lg-4 pt-lg-3">
              <form method="post" action="<?php echo $currentUrl; ?>?na=s" onsubmit="return newsletter_check(this)">
                <div>
                  <div class="d-inline-block tnp-field tnp-field-email f-16 f-sb"><input class="tnp-email w-100 border-0 py-2 px-2" type="email" name="ne" placeholder="<?php _e('[:pl]Wpisz swój adres email[:en]Enter your e-mail address[:de]Geben Sie Ihre E-Mail-Adresse ein.[:]'); ?>" required></div>
                  <div class="d-inline-block tnp-field tnp-field-button"><input class="tnp-submit text-white bg-black border-0 f-16 f-sc f-b py-2 px-3 px-lg-5" type="submit" value="<?php _e('[:pl]zapisz się[:en]subscribe[:de]abonnieren[:]'); ?>"></div>
                </div>
                <div class="tnp-field tnp-field-privacy f-12 f-sb pt-1 pt-lg-3"><label><input type="checkbox" name="ny" required class="tnp-privacy"> <a target="_blank" rel="noopener" href="<?php echo get_permalink($ppolicy->ID); ?>"><?php _e('[:pl]Przeczytałem i akceptuję postanowienia ogólne i politykę prywatności[:en]I have read and accept the general conditions and the privacy policy[:de]Ich habe die allgemeinen Bestimmungen und die Datenschutzerklärung gelesen und akzeptiere sie.[:]'); ?></a></label></div>
              </form>
            </div>
          </div>
        </div>
      </div>

    </div><!-- .row end -->
    <?php if($linesQuery->have_posts()): while($linesQuery->have_posts()): $linesQuery->the_post(); 
          $lineName = get_field('name', $post->ID);
    ?>
    <div class="row border-1 border-gray border-bottom">
      <div class="container">
        <div class="row my-4 py-3">
          <div class="col-12 col-md-6 col-lg-4 line-link">
            <a class="line-link" href="<?php echo get_permalink($post->ID); ?>">
              <div class="row  <?php echo $lineName; ?> mx-lg-0">
                <div class="col-12 col-md-6 h-30 h-md-60 bg-gray1 text-center align-middle">
                  <div class="d-inline-block h-100 align-middle"></div>
                  <div class="align-middle d-inline-block w-95">
                    <div class="logo-<?php echo $lineName; ?>-bl-s"></div>
                  </div>
                </div>
                <div class="col-12 col-md-6 h-45 h-md-60 bg-full" style="background-image:url('<?php echo get_the_post_thumbnail_url($post->ID, 'line_thumb'); ?>'); "></div>
                <div class="col-12 f-lg-16 f-b h-25 h-md-auto pt-2 pt-md-0"><?php echo get_field('type', $post->ID); ?></div>
              </div>
            </a>
          </div>
          <div class="col-12 col-md-4 col-lg-5 offset-lg-1 h-50v py-4 py-md-0 pl-md-5 pl-lg-0">
            <div class="f-16 f-sc f-b "><?php _e('[:pl]PLIKI DO POBRANIA[:en]DOWNLOADABLE FILES[:de]DATEIEN ZUM HERUNTERLADEN[:]'); ?></div>
            <div class="bottom-0">
              <?php if(have_rows('files', $post->ID)): while(have_rows('files', $post->ID)): the_row(); 
                    if(in_array('architect', get_sub_field('file_where'))): 
            ?>
              <a class="d-block mt-2" href="<?php echo get_sub_field('file'); ?>" target="_blank" rel="noopener">
                <div class="download-ico d-inline-block f-12 f-sc f-sb mr-3"><?php echo get_sub_field('short'); ?></div><div class="d-inline-block f-16 f-sb align-bottom"><?php echo get_sub_field('file_name'); ?></div>
              </a>
            <?php endif; endwhile; endif; ?>
            </div>
          </div>
          <div class="col-12 col-md-2 f-12 f-sb">
            <div class="h-100 d-inline-block"></div><a class="d-inline-block align-bottom f-16" href="<?php echo get_permalink(); ?>"><u><?php _e('[:pl]więcej o produkcie[:en]More about the product[:de]Mehr über das Produkt[:]')?></u></a>
          </div>
        </div>
      </div>
    </div>
    <?php    endwhile;    endif;    wp_reset_query(); ?>
  </div><!-- Container end -->
</div>

<?php get_footer(); ?>