(function($){
  $(document).ready(function(){
    
    var $gallery = $('#gallery');
    var $variants = $('#variants');
    var $loader = $('#loader');
    var currentMenuItem = $('#main-menu .current-menu-item a');
    
//    
//    model gallery functions
//    
//    
    
    var toggleVisible = function(event) {
      var sourceClass = event.data.sourceClass;
      var targetClass = event.data.targetClass;
      var targetId = event.data.targetId;
      var dataParam = event.data.dataParam;
      var clickedParam = $(this).data(dataParam);
      $(sourceClass).removeClass('active');
      $(this).addClass('active');
      
      $(targetClass).addClass('d-none');
      $(targetId+clickedParam).removeClass('d-none');
      if(sourceClass === '.select-variant'){
        $('.gal-prev, .gal-next').removeClass('d-lg-block');
        $('.switch-' + clickedParam).addClass('d-lg-block');
      }
    };
    
    var showFirst = function(){
      var variant = $(this).data('variant');
      var firstGalItem = $('#gallery_'+variant+' .gallery-page').children().first().data('galitem');
      $('.big-photo').addClass('d-none');
      $('#mb_'+firstGalItem).removeClass('d-none');
    };
    
    var galleryClicks = function(){
      $('.select-variant').click({sourceClass: '.select-variant', targetClass: '.gallery.row', targetId: '#gallery_', dataParam: 'variant'}, toggleVisible);
      $('.select-variant').click(showFirst);
      $('.gallery-item').click({sourceClass: '.gallery-item', targetClass: '.big-photo', targetId: '#mb_', dataParam: 'galitem'}, toggleVisible);
      $('.gal-prev.active, .gal-next.active').click(scrollGallery);
    };
    
    var galleryLoadingStyles = function(state){
      if(state === 'load'){
        $variants.addClass('loading');
        $gallery.addClass('loading');
      }
      else{
        $variants.removeClass('loading');
        $gallery.removeClass('loading');
      }
    };
    
    var changeModel = function(event){
      event.preventDefault();
      $('.select-model').removeClass('active');
      $(this).addClass('active');
      var id = $(this).data('modelid');
      var data = {
        'action':'get_model',
        'model_id': id
      };
      galleryLoadingStyles('load');
      
      setTimeout(function(){
        $variants.html('');
        $gallery.html('<div class="loader mt-7">Loading... </div>');
        galleryLoadingStyles();
      }, 500);
      
      
      $.post(WpAjax.ajaxUrl, data, ajaxResponse);
    };
    
    var setGalleryWidth = function(){
      var percent = 100;
      if($(window).width() < 992){
       percent = 101;
      }
      $('.gallery.row').each(function () {
        $(this).css('width', $(this).find('.gallery-page').length * percent + '%');
      });
    };
    
    var scrollGallery = function(){
      var variant = $(this).data('variant');
      var direction = $(this).data('dir');
      var gallery = $('#gallery_'+variant);
      var galleryPages = gallery.find('.gallery-page').length;
      var step = 474;
      var currentPosition = gallery.attr('style').match(/\([\-0-9]+/);
      var newPosition = 0;
      currentPosition = currentPosition ? Number(currentPosition[0].replace('(', '')) : 0;
      newPosition = direction === 'right' ? currentPosition - step : currentPosition + step;
      
      gallery.css({
        'transform': 'translateX(' + newPosition + 'px)',
        '-webkit-transform': 'translateX(' + newPosition + 'px)'
      });
      setGalleryArrows(newPosition, galleryPages, step, variant);
    };
    
    
    var setGalleryArrows =function(galleryPosition, galleryPages, step, variant){
      var prev = $('#gal-nav-p-'+variant);
      var next = $('#gal-nav-n-'+variant);
      var maxScroll = -(step * (galleryPages -1));
      if(galleryPosition === 0){
        prev.removeClass('active');
        next.addClass('active');
      }
      else if(galleryPosition === maxScroll){
        prev.addClass('active');
        next.removeClass('active');
      }
      else{
        prev.addClass('active');
        next.addClass('active');
      }
      $('.gal-prev, .gal-next').off('click');
      $('.gal-prev.active, .gal-next.active').click(scrollGallery);
    };

    var setInnerSiteVariants = function (template) {
      var variants = document.querySelectorAll('[data-templates]');
      for (var i = 0; i < variants.length; i++) {
        var variant = variants[i];
        var templates = variant.dataset.templates.split(',');
        if (templates.indexOf(template) > -1) {
          variant.classList.remove('d-none');
        } else {
          variant.classList.add('d-none');
        }
      }
    };
    
    var ajaxResponse = function(response){
      var resp = JSON.parse(response);
      
      galleryLoadingStyles('load');
      
      setTimeout(function(){
        $variants.html(resp.variants);
        $gallery.html(resp.gallery);
        galleryLoadingStyles();
        galleryClicks();
        setInnerSiteVariants(resp.template);
        document.title = resp.pageTitle;
        window.history.pushState({"html":resp.gallery,"pageTitle":resp.pageTitle},resp.pageTitle, resp.urlPath);
        galleryPopupInit('.plus', 'front-popup');
        setGalleryWidth();
      }, 500);
    };
    
    galleryClicks();
    
    $('.select-model').click(changeModel);
    $('.handle').click({sourceClass: '.handle', targetClass: '.handle-big', targetId: '#handle_', dataParam: 'handle_name'}, toggleVisible);
    
//    
//    
//    scroll effects and functions
//    
//    
    var stickyHeader = function(scroll){
      var body = $('body');
      if(scroll > 0){
        body.addClass('sticky');
      }
      else{
        body.removeClass('sticky');
      }
      
    };
    
    var getBackgroundSize = function(elem) {
      // This:
      //       * Gets elem computed styles:
      //             - CSS background-size
      //             - element's width and height
      //       * Extracts background URL
      // credits http://jsfiddle.net/gp4e9d3z/3/
      var computedStyle = getComputedStyle(elem),
              image = new Image(),
              src = computedStyle.backgroundImage.replace(/url\((['"])?(.*?)\1\)/gi, '$2'),
              cssSize = computedStyle.backgroundSize,
              elemW = parseInt(computedStyle.width.replace('px', ''), 10),
              elemH = parseInt(computedStyle.height.replace('px', ''), 10),
              elemDim = [elemW, elemH],
              computedDim = [],
              ratio;
      // Load the image with the extracted URL.
      // Should be in cache already.
      image.src = src;
      // Determine the 'ratio'
      ratio = image.width > image.height ? image.width / image.height : image.height / image.width;
      // Split background-size properties into array
      cssSize = cssSize.split(' ');
      // First property is width. It is always set to something.
      computedDim[0] = cssSize[0];
      // If height not set, set it to auto
      computedDim[1] = cssSize.length > 1 ? cssSize[1] : 'auto';
      if (cssSize[0] === 'cover') {
        // Width is greater than height
        if (elemDim[0] > elemDim[1]) {
          // Elem's ratio greater than or equal to img ratio
          if (elemDim[0] / elemDim[1] >= ratio) {
            computedDim[0] = elemDim[0];
            computedDim[1] = 'auto';
          } else {
            computedDim[0] = 'auto';
            computedDim[1] = elemDim[1];
          }
        } else {
          computedDim[0] = 'auto';
          computedDim[1] = elemDim[1];
        }
      } else if (cssSize[0] === 'contain') {
        // Width is less than height
        if (elemDim[0] < elemDim[1]) {
          computedDim[0] = elemDim[0];
          computedDim[1] = 'auto';
        } else {
          // elem's ratio is greater than or equal to img ratio
          if (elemDim[0] / elemDim[1] >= ratio) {
            computedDim[0] = 'auto';
            computedDim[1] = elemDim[1];
          } else {
            computedDim[1] = 'auto';
            computedDim[0] = elemDim[0];
          }
        }
      } else {
        // If not 'cover' or 'contain', loop through the values
        for (var i = cssSize.length; i--; ) {
          // Check if values are in pixels or in percentage
          if (cssSize[i].indexOf('px') > -1) {
            // If in pixels, just remove the 'px' to get the value
            computedDim[i] = cssSize[i].replace('px', '');
          } else if (cssSize[i].indexOf('%') > -1) {
            // If percentage, get percentage of elem's dimension
            // and assign it to the computed dimension
            computedDim[i] = elemDim[i] * (cssSize[i].replace('%', '') / 100);
          }
        }
      }
      // If both values are set to auto, return image's 
      // original width and height
      if (computedDim[0] === 'auto' && computedDim[1] === 'auto') {
        computedDim[0] = image.width;
        computedDim[1] = image.height;
      } else {
        // Depending on whether width or height is auto,
        // calculate the value in pixels of auto.
        // ratio in here is just getting proportions.
        ratio = computedDim[0] === 'auto' ? image.height / computedDim[1] : image.width / computedDim[0];
        computedDim[0] = computedDim[0] === 'auto' ? image.width / ratio : computedDim[0];
        computedDim[1] = computedDim[1] === 'auto' ? image.height / ratio : computedDim[1];
      }
      // Finally, return an object with the width and height of the
      // background image.
      return {
        width: computedDim[0],
        height: computedDim[1]
      };
    };
    
    var parallax = function(scroll){
      $('.bg-parallax').each(function(){
        var speed = $(this).data('speed');
        var start = $(this).data('start');
        var thisHeight = $(this).height();
        var bgHeight = getBackgroundSize(this).height;
        var thisPosition = $(this).offset().top;
        var windowHeight = $(window).height();
        var bgPosition =  speed * (thisPosition - scroll - (windowHeight * start));


        if((thisPosition - scroll) < windowHeight * start && -bgPosition < bgHeight - thisHeight){
          $(this).css('background-position', '50% ' + bgPosition + 'px');
        }
      });
    };
    
    var onScroll = function(){
      var currentScroll = $(this).scrollTop();
      stickyHeader(currentScroll);
      parallax(currentScroll);
    };
    
    $(window).scroll(onScroll);
    
    var galleryPopupInit = function(handleClass, popupId){
      $(handleClass).magnificPopup({
        type: 'image',
        preloader: true,
        gallery: {
          enabled: true,
          preload: [0, 3]
        },
        callbacks: {
          open: function () {
            $('.mfp-wrap').attr('id', popupId);
            $('.mfp-wrap').append('<div class="pop-info f-sc f-b f-16"><div class="pop-name"></div><div class="pop-variant"></div></div>');
            $('.pop-name').text('');
            $('.pop-variant').text('');
          },
          imageLoadComplete: function () {
            $('.pop-name').text($(this.currItem.el[0]).data('itemname'));
            $('.pop-variant').text($(this.currItem.el[0]).data('variant'));
          }
        }
      });
    };
    
    var menuLineHover = function(){
      if(!$(this).parent().parent().hasClass('dropdown-menu')){
        var left = $(this).position().left;
        var width = $(this).width();
        setTimeout(function(){ 
          setMenuLine(left, width);
        }, 300);
      }
      
    };
    
    var menuLineActive = function(){
      setTimeout(function(){
        if (typeof currentMenuItem[0] !== 'undefined') {
          if (currentMenuItem.parent().parent().hasClass('dropdown-menu')) {
            var left = $('#main-menu .current-menu-parent a').position().left;
            var width = $('#main-menu .current-menu-parent a').width();
            setMenuLine(left, width);
          } else {
            var left = currentMenuItem.position().left;
            var width = currentMenuItem.width();
            setMenuLine(left, width);
          }
        } else {
          setMenuLine(0, 0);
        }
      }, 300);
    };
    
    var setMenuLine = function(left, width){
      var width = width !== 0 ? width + 20 : 0;
      $('#header-line').css({
        'width': width,
        'left': left - 1
      });
    };
    
    $(document).ready(function(){
      $loader.addClass('loaded');
      
      if(navigator.userAgent.match(/iPhone|iPad|iPod/i)){
       $('.bg-full').removeClass('bg-fixed'); 
      }
      
      setTimeout(function(){
        $loader.addClass('d-none');
      
        if($(window).width() >= 992){
          menuLineActive();
        }
      
      }, 500);
      
      galleryPopupInit('.plus', 'front-popup');
      galleryPopupInit('.gall-item-a', 'back-popup');
      setGalleryWidth();
      
    
      $('.model-sub-menu a').click(function(e){
        e.preventDefault();
        var target = $(this).attr('href');
        $('html, body').animate({scrollTop: $(target).offset().top - 100}, 1000);
      });
    });
    
    $('.navbar a, .footer-menu a, .footer-brand, .model_thumb_big a').click(function(e){
      if ((e.target.target !== '_blank') && $(this).attr('href') !== '#'){
        $loader.removeClass('d-none');
        setTimeout(function () {
          $loader.removeClass('loaded');
        }, 50);
      }
    });
    
    $('#main-menu').prepend('<div id="header-line"></div>');
    
    if($(window).width() >= 992){
      $('#main-menu .menu-item a').hover(menuLineHover, menuLineActive);
      
      $('.dropdown').off('click');
      $('.dropdown').hover(function(){
        $(this).toggleClass('show');
        $(this).find('.dropdown-menu').toggleClass('show');
        $(this).find('.dropdown-toggle').attr('aria-expanded', 'true');
      },
      function(){
        $(this).toggleClass('show');
        $(this).find('.dropdown-menu').toggleClass('show');
        $(this).find('.dropdown-toggle').attr('aria-expanded', 'false');
      });
    }
    
  });
  
})(jQuery);

