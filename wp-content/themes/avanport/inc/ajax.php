<?php

add_action('wp_ajax_get_model', 'get_model');
add_action('wp_ajax_nopriv_get_model', 'get_model');


function get_model(){
  $modelId = intval($_POST['model_id']);
  
  ob_start(); ?>

    <div class="f-12 pt-lg-3 px-3 px-lg-0 first"><?php if(have_rows('gallery_b', $modelId)) {_e('[:pl]wybierz wariant[:en]variation[:de]variant[:]');} ?></div>
    <div class="f-16 f-sc f-b pt-lg-2 mt-lg-1">
    <?php 
    if(have_rows('gallery_b', $modelId)){echo '<span class="select-variant active px-lg-2" data-variant="a">A</span><span class="select-variant px-lg-2" data-variant="b">B</span>';}
    if(have_rows('gallery_c', $modelId)){echo '<span class="select-variant px-lg-2" data-variant="c">C</span>';}
    ?>
    </div>  

<?php 
  $variantsHTML = ob_get_contents();
  ob_end_clean();
  
  ob_start(); ?>

        <div class="col-12 col-lg-7 border-1 border-gray border-left border-right px-0">
          <?php
          $variants = Array('a', 'b', 'c');
          $fb = false;
          foreach ($variants as $v):
            if (have_rows('gallery_' . $v, $modelId)):
              $n = 0;
              $haveMoreVariants = have_rows('gallery_b', $modelId) ? true : false;
              $galleryRows = get_field('gallery_' . $v, $modelId);
              foreach ($galleryRows as $row) {
                $ids[] = $row['photo'];
              }
              $cache = get_posts(array('post_type' => 'attachment', 'numberposts' => -1, 'post__in' => $ids));
              foreach($galleryRows as $row):  
                $photoId = $row['photo'];
                $photoThumb = wp_get_attachment_image_src($photoId, 'model_thumb')[0];
                $fullPhoto = wp_get_attachment_image_src($photoId, 'page_width')[0];
                $photo = wp_get_attachment_image_src($photoId, 'model_big')[0];
                $pclass = $fb ? 'd-none' : '';
                $dataVariant = $haveMoreVariants ? __('[:pl]wariant [:en]variation [:de]variant [:]').' '.$v : '';
                ?>
                <div id="mb_<?php echo basename($photoThumb, '.jpg'); ?>" class="h-100 text-center big-photo <?php echo $pclass; ?>" >
              <img src="<?php echo $photo; ?>" alt="<?php echo basename($photo, '.jpg'); ?>">
              <a class="plus border-1 border-gray border-top border-left f-22" href="<?php echo $fullPhoto; ?>" data-itemname="<?php echo get_the_title($modelId); ?>" data-variant="<?php echo $dataVariant; ?>">+</a>
            </div>
                <?php $fb = true; $n++; endforeach; endif; endforeach;?>
        </div>
          <div class="col-12 col-lg-5 border-1 border-gray border-right gallery-wrapper">
          <?php
          $fv = false;
          foreach ($variants as $v):
            $vclass = $fv ? 'd-none' : '';
            $bclass = $fv ? '' : 'd-lg-block';
            if (have_rows('gallery_' . $v, $modelId)):
              $en = 1;
              $allItems = count(get_field('gallery_'.$v, $modelId));
              ?>
              <div class="gallery row <?php echo $vclass; ?>" id="gallery_<?php echo $v; ?>">
                <div class="gallery-page">
                <?php
                $galleryRows = get_field('gallery_'.$v, $modelId);
                foreach($galleryRows as $row): 
                  $photoId = $row['photo'];
                  $photo = wp_get_attachment_image_src($photoId, 'model_thumb')[0];
                  $br = $en % 3 === 0 ? '' : 'border-right';
                  ?>
                  <div class="gallery-item col-lg-4 px-0 text-center border-1 border-gray border-bottom <?php echo $br; ?>" data-galitem="<?php echo basename($photo, '.jpg'); ?>"><img class="w-100" src="<?php echo $photo; ?>" alt="<?php echo basename($photo, '.jpg'); ?>"></div>
         <?php 
              if(($en % 12 === 0) && $en !== $allItems){echo '</div><div class="gallery-page">';}
              
              $en++; 
              endforeach; 
              $en--;
              if($en % 12 !== 0){
                $ceil = ceil($en / 12);
                $add = ($ceil * 12) - $en;
                
                for($i = 0; $i < $add; $i++){
                  echo '<div class="d-none d-lg-block gallery-item col-4 px-0 invisible"></div>';
                }
              }
              if($en === $allItems){echo '</div>';}
              ?>
            </div>
              <?php if($allItems > 12): ?>
              <span class="d-none <?php echo $bclass; ?> gal-prev border-1 border-gray border-top border-left f-22 switch-<?php echo $v; ?>" id="gal-nav-p-<?php echo $v?>" data-dir="left" data-variant="<?php echo $v; ?>"><</span><span class="d-none <?php echo $bclass; ?> gal-next border-1 border-gray border-top border-left f-22 switch-<?php echo $v; ?> active" id="gal-nav-n-<?php echo $v?>" data-dir="right" data-variant="<?php echo $v; ?>">></span>
              <?php endif; ?>
          
          <?php endif; $fv = true; endforeach;?>
          </div>  


  <?php
  
  $gallery = ob_get_contents();
  ob_end_clean();
  echo json_encode(array(
    'variants'=> $variantsHTML, 
    'gallery' => $gallery, 
    'urlPath' => get_permalink($modelId), 
    'pageTitle'=> get_the_title($modelId), 
    'template' => get_field('template', $modelId)
  ));
  wp_die();
}

