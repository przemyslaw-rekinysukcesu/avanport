<?php

class CustomPostType {
  
  public $name;
  public $singName;
  public $slug;
  public $menuPosition;
  
  public function __construct($name, $singName, $slug, $menuPosition) {
//    global $post;
    $this->name = $name;
    $this->singName = $singName;
    $this->slug = $slug;
    $this->menuPosition = $menuPosition;
    
    add_action('init', array($this, '_create_post_type'), 5);

//    if($slug === 'offer'){
//      add_action('wpcf7_before_send_mail', array($this, '_saveFormData'),10,2);
//      add_filter('manage_my_post_type_' . $this->slug . '_posts_columns', array($this, '_columns'), 10, 2);
//      add_action('manage_my_post_type_' . $this->slug . '_posts_custom_column', array($this, '_column_data'), 11, 2);
//      add_action('manage_edit-my_post_type_' . $this->slug . '_sortable_columns', array($this, '_sort_columns'), 11, 2);
//      add_action('pre_get_posts', array($this, '_set_orderby'));
//    }
//    if($slug === 'app'){
//      add_filter('manage_my_post_type_'.$this->slug.'_posts_columns', array($this, '_columns'), 10, 2);
//      add_action('manage_my_post_type_'.$this->slug.'_posts_custom_column', array($this, '_column_data'), 11, 2);
//      add_action('manage_edit-my_post_type_'.$this->slug.'_sortable_columns', array($this, '_sort_columns'), 11, 2);
//      add_action('pre_get_posts', array($this, '_set_orderby'));
//    }
  }

  function _create_post_type() {
    $labels = array(
        'name' => $this->name,
        'singular_name' => $this->singName,
        'menu_name' => $this->name,
        'name_admin_bar' => $this->singName,
        'add_new' => 'Dodaj',
        'add_new_item' => 'Dodaj',
        'new_item' => 'Nowy',
        'edit_item' => 'Edytuj',
        'view_item' => 'Zobacz',
        'all_items' => 'Wszystkie',
        'search_items' => 'Szukaj',
        'parent_item_colon' => 'Nadrzędny',
        'not_found' => 'Nic nie znaleziono',
        'not_found_in_trash' => 'Nic nie znaleziono w koszu'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_nav_menus' => true,
        'show_in_menu' => true,
        'show_in_admin_bar' => true,
        'menu_position' => $this->menuPosition,
        'menu_icon' => 'dashicons-admin-appearance',
        'capability_type' => 'post',
        'hierarchical' => false,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'custom-fields'),
        'has_archive' => true,
        'rewrite' => array('slug' => $this->slug),
        'query_var' => true,
        'taxonomies' => array('post_tag')
    );

    register_post_type('my_'.$this->slug, $args);
  }

  
//  function _saveFormData($cf7) {
//    
//    $submission = WPCF7_Submission::get_instance();
//
//    if ($submission) {
//      $posted_data = $submission->get_posted_data();
//      $currentPostId = $posted_data['_wpcf7_container_post'];
//    }
//
//    $postArray = array(
//        'post_title' => get_the_title($currentPostId),
//        'post_content' => '',
//        'post_type' => 'my_post_type_app',
//        'post_status' => 'private',
//        'meta_input' => array(
//            'firma' => get_field('client', $currentPostId),
//            'city' => get_field('city', $currentPostId),
//            'e-mail' => $posted_data['your-email'],
//            'tel' => $posted_data['phone'],
//            'title' => $currentPostId
//        )
//    );
//    $newPostId = wp_insert_post($postArray);
//    $uploadedFiles = $submission->uploaded_files();
//    $n = 0;
//    foreach ($uploadedFiles as $uFile) {
//      if ($n === 0) {
//        $this->_saveFile($newPostId, $uFile, true);
//      } else {
//        $this->_saveFile($newPostId, $uFile, false, $n);
//      }
//      $n++;
//    }
//  }
//
//  private function _saveFile($postId, $uFile, $first, $n = '') {
//    $filename = basename($uFile);
//    $getImageFile = wp_upload_bits($filename, null, file_get_contents($uFile));
//    $getImageFile = $getImageFile['file'];
//
//    $wp_filetype = wp_check_filetype($filename, null);
//
//    $attachment_data = array(
//        'post_mime_type' => $wp_filetype['type'],
//        'post_parent' => $postId,
//        'post_title' => sanitize_file_name($filename),
//        'post_content' => '',
//        'post_status' => 'inherit'
//    );
//
//    $attach_id = wp_insert_attachment($attachment_data, $getImageFile, $pstId);
//    if (!is_wp_error($attach_id)) {
//      require_once(ABSPATH . "wp-admin" . '/includes/image.php');
//      $attachment_data = wp_generate_attachment_metadata($attach_id, $getImageFile);
//      wp_update_attachment_metadata($attach_id, $attachment_data);
//    }
//    if ($first)
//      set_post_thumbnail($postId, $attach_id);
//      update_post_meta($postId, 'cv' . $n, $attach_id);
//  }
  
//  function _columns($columns) {
//    unset($columns['author']);
//    unset($columns['tags']);
//    unset($columns['language']);
//    unset($columns['gadwp_stats']);
//    return array_merge(
//            $columns, array(
//        'client' => 'Klient',
//        'city' => 'Miasto',
////        'e-mail' => 'E-mail'
//            )
//    );
//  }
//
//  function _column_data($column, $post_id) {
//    switch ($column) {
//      case 'client' :
//        $client = get_post_meta($post_id, 'firma', 1) ? get_post_meta($post_id, 'firma', 1) : get_post_meta($post_id, 'client', 1);
//        echo get_the_title($client[0]);
//        break;
//      case 'city' :
//        echo get_post_meta($post_id, 'city', 1);
//        break;
//      case 'e-mail' :
//        echo get_post_meta($post_id, 'e-mail', 1);
//        break;
//    }
//  }
//
//  function _sort_columns($columns) {
//    $columns['client'] = 'client';
//    $columns['city'] = 'city';
//    $columns['e-mail'] = 'e-mail';
//
//    return $columns;
//  }

//  function _set_orderby($query) {
//    if (!is_admin())
//      return;
//
//    $orderby = $query->get('orderby');
//
//    switch ($orderby) {
//      case 'client' :
//        $query->set('meta_key', 'client');
//        $query->set('orderby', 'meta_value');
//        break;
//      case 'city' :
//        $query->set('meta_key', 'city');
//        $query->set('orderby', 'meta_value');
//        break;
//      case 'e-mail' :
//        $query->set('meta_key', 'e-mail');
//        $query->set('orderby', 'meta_value');
//        break;
//    }
//
//    // if( 'slice' == $orderby ) {
//    //     $query->set('meta_key','slices');
//    //     $query->set('orderby','meta_value_num');
//    // }
//  }

}
