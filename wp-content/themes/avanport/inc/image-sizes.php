<?php
add_image_size('line_photo', 606, 540, false);
add_image_size('line_thumb', 172, 132, false);
add_image_size('page_width', 1900, 9999, false);
add_image_size('model_big', 678, 600, false);
add_image_size('in_big', 600, 9999, false);
add_image_size('model_thumb', 136, 175, false);
add_image_size('model_thumb_big', 270, 360, false);
add_image_size('handle_thumb', 150, 175, false);
add_image_size('handle_big', 445, 167, false);
add_image_size('catalog_thumb', 210, 300, false);
add_image_size('table_thumb', 260, 300, false);
add_image_size('stone_thumb', 530, 220, false);
add_image_size('handrail', 346, 475, false);

