<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<div class="col-12 col-md-6">
	<article <?php post_class(); ?> id="post-<?php the_ID(); ?>" itemprop="blogPost" itemscope="" itemtype="https://schema.org/BlogPosting">
		<div class="blog__item mb-5"> 
			<div class="blog__title blog__title--img">
				<a href="<?= esc_url( get_permalink() ) ?>">
					<div class="blog__img">
						<?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
					</div>
				</a>
				<?php /*
				<div class="blog__category">
					<a class="btn btn-primary btn-sm" href="<?= get_category_link(get_the_category($post->ID)[0]->cat_ID) ?>" title="<?= get_the_category($post->ID)[0]->cat_name ?>">
						<?= get_the_category($post->ID)[0]->cat_name?>
					</a>
				</div>
				*/ ?>

				<a href="<?= esc_url( get_permalink() ) ?>">			
					<div class="blog__img-text">
						<div class="row align-items-center">
							<div class="col-12 col-sm">
								<div class="blog__heading my-2">
									<?php the_title('<h2 itemprop="headline"><strong>','</strong></h2>'); ?>
								</div>
							</div>
								
							<div class="col-12 col-sm-auto">
								<div class="blog__date">
									<small>
										<meta itemprop="datePublished" content="<?= get_the_date( 'c' ) ?>">
										<?= iconv("ISO-8859-2","UTF-8",ucfirst(strftime('%d.%m.%Y',strtotime(get_the_date( 'c' )))));?>
									</small>
								</div>
							</div>
						</div>

						
					</div>
				</a>
			</div>

			<?php if ( 'post' == get_post_type() ) : ?>
				<div>
					<p class="blog__text">
						<?= get_the_excerpt(); ?>
					</p>
				</div>
			<?php endif; ?>
		</div>
	</article>
</div>
