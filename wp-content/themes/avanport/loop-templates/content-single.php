<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>" itemprop="blogPost" itemscope="" itemtype="https://schema.org/BlogPosting">
	<div class="blog__item mb-6"> 
		<?php /*
		<div class="blog__title blog__title--img">
			<div class="blog__img">
				<?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
			</div>
				
			<div class="blog__category">
				<a class="btn btn-primary btn-sm" href="<?= get_category_link(get_the_category($post->ID)[0]->cat_ID) ?>" title="<?= get_the_category($post->ID)[0]->cat_name ?>">
					<?= get_the_category($post->ID)[0]->cat_name?>
				</a>
			</div>
		</div>
		<div class="blog__title">
			<div class="blog__img-text">
				<div class="blog__date">
					<meta itemprop="datePublished" content="<?= get_the_date( 'c' ) ?>">
					<?= iconv("ISO-8859-2","UTF-8",ucfirst(strftime('%d.%m.%Y',strtotime(get_the_date( 'c' )))));?>
				</div>
				
				<div class="blog__heading">
					<?php the_title('<h1 itemprop="headline">','</h1>'); ?>
				</div>
			</div>
		</div>
		*/?>

		<div class="blog__text">
			<?= the_content(); ?>
		</div>
		<div class="mt-6">
			<a class="button-black-border" href="https://www.avanport.pl/category/blog">&laquo; Wszystkie wpisy</a>
		</div>
	</div>
</article>